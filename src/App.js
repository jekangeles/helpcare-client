import React, { useState, useEffect, Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import jwt_decode from "jwt-decode"

import Toolbar from "./components/layouts/Toolbar/Toolbar"
import SideDrawer from "./components/layouts/SideDrawer/SideDrawer"
import Backdrop from "./components/layouts/Backdrop/Backdrop"
import LandingPage from "./components/LandingPage/LandingPage"
import Login from "./components/forms/Login/Login"
import Register from "./components/forms/Register/Register"
import Auth from "./components/Auth"

import DashboardPatient from "./components/Dashboard/DashboardPatient/DashboardPatient"
import DashboardProvider from "./components/Dashboard/DashboardProvider/DashboardProvider"
import DashboardAdmin from "./components/Dashboard/DashboardAdmin/DashboardAdmin"

function App() {

	const [user, setUser] = useState({})
	const [token, setToken] = useState("")

	useEffect( () => {
		if(token) {
			let decoded = jwt_decode(token)
			let now = new Date()
			if(decoded.exp <= now.getTime()) {
				localStorage.clear()
				window.location.href='/login'
			}
		}

		setUser(JSON.parse(localStorage.getItem("user")))
		setToken(localStorage.getItem("token"))
	}, [])

	const [sideDrawerOpen, setSideDrawerOpen] = useState(false)

	let backdrop;
	if (sideDrawerOpen) {
		backdrop = <Backdrop setSideDrawerOpen={setSideDrawerOpen}/>
	}

	let navbar = (
		<Fragment>
			<Toolbar setSideDrawerOpen={setSideDrawerOpen} />
			<SideDrawer 
				sideDrawerOpen={sideDrawerOpen}
				setSideDrawerOpen={setSideDrawerOpen}
			/>
			{backdrop}
		</Fragment>
	)

	const logoutHandler = () => {
		localStorage.clear()
		setUser({})
		setToken("")
		window.location.href="/"
	}

	// let dashboard;
	// if(user){
	// 	if(user.role === "admin") {
	// 		dashboard = (
	// 			<DashboardAdmin
	// 				logoutHandler={logoutHandler}
	// 				user={user}
	// 				token={token}
	// 			/>
	// 		)
	// 	} else if (user.role === "provider") {
	// 		dashboard = (
	// 			<DashboardProvider
	// 				logoutHandler={logoutHandler}
	// 				user={user}
	// 				token={token}
	// 			/>
	// 		)
	// 	} else if (user.role === "patient") {
	// 		dashboard = ( 
	// 			<DashboardPatient 
	// 				logoutHandler={logoutHandler}
	// 				user={user}
	// 				token={token}
	// 			/>
	// 		)
	// 	} else {
	// 		dashboard = <Auth />
	// 	}
	// }

    return (
        <Router>
        	<div style={{height: '100%'}}>
	            <main>
		            <Switch>
		            	<Route exact path='/'>
		            		{navbar}
		                	<LandingPage />
		            	</Route>
		            	<Route path='/login'>
		                	<Login />
		            	</Route>
		            	<Route path='/register'>
		                	<Register />
		            	</Route>
		            	<Route path="/patients/dashboard">
		            		{user && token ? 
		            			<DashboardPatient 
		            				logoutHandler={logoutHandler}
		            				user={user}
		            				token={token}
		            			/> : <Auth /> 
		            		}
		            	</Route>
		            	<Route path="/providers/dashboard">
		            		{user && token ? 
		            			<DashboardProvider 
		            				logoutHandler={logoutHandler}
		            				user={user}
		            				token={token}
		            			/> : <Auth /> 
		            		}
		            	</Route>
		            	<Route path="/admin/dashboard">
		            		{user && token ? 
		            			<DashboardAdmin 
		            				logoutHandler={logoutHandler}
		            				user={user}
		            				token={token}
		            			/> : <Auth /> 
		            		}
		            	</Route>
		            </Switch>
	            </main>
        	</div>
        </Router>
    );
}

export default App;
