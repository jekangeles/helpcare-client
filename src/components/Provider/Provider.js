import React, { useState, useEffect, Fragment } from "react"
import { useParams, Link } from "react-router-dom"

import { URL } from "../../config"

import "./Provider.css"

const Provider = () => {
	let {providerId} = useParams()
	const [provider, setProvider] = useState({})

	useEffect( () => {
		fetch(`${URL}/providers/${providerId}`)
		.then(res => res.json())
		.then(data => {
			setProvider(data)
		})
	}, [])

	return (
		<Fragment>
			<div className="content">
				<h2 className="content-title"><Link to="/patients/dashboard/catalog"><i className="fas fa-chevron-left"></i> Back</Link></h2>
				<div className="view-details-container">
					<div className="view-details-image-container">
						<img src={`${URL}${provider.image}`} />
					</div>
					<div className="view-details-details-container">
						<h2>Dr. {provider.firstName} {provider.lastName}</h2>
						<em>{provider.fieldName}</em>
						<p>Appointment Fee: &#8369;{provider.appointmentFee}</p>
						<p>{provider.clinicName}</p>
						<p>{provider.clinicAddress}</p>
						<button className="button-book"><Link to={`/patients/dashboard/book/${provider._id}`}>Book Appointment</Link></button>
					</div>
				</div>
			</div>
		</Fragment>
	)
}

export default Provider