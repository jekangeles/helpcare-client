import React, { useState, useEffect, Fragment } from "react"
import { Link } from "react-router-dom"
import { URL } from "../../../config"

import "./Catalog.css"

const AppointmentPatient = () => {
	const [fields, setFields] = useState([])
	const [providers, setProviders] = useState([])

	useEffect( () => {
		fetch(`${URL}/fields`)
		.then(res => res.json())
		.then(data => {
			setFields(data)
		})

		fetch(`${URL}/providers`)
		.then(res => res.json())
		.then(data => {
			setProviders(data)
		})

	}, [])


	return (
		<Fragment>
			<div className="content">
				<div className="content-title">
					<h2>Book an appointment</h2>
				</div>
				<div className="content-nav">
					<div className="content-search">
						<input className="input-field-2" type="text" />
					</div>
					<div className="content-filter">
						<select className="input-field-2" name="fieldId">
							<option>All</option>
							{ fields.map(field => (
								<option value={field._id} key={field._id}>{field.name}</option>
							))}
						</select>
					</div>
				</div>
				<hr className="content-hr"/>
				<div className="content-gallery">
					{ providers.map(provider => (
						<div className="content-item" key={provider._id}>
							<div className="content-image-container">
								<Link to={`/patients/dashboard/providers/${provider._id}`}><img src={`${ URL }${provider.image}`} alt="provider-portrait" /></Link>
							</div>
							<div className="content-details">
								<h3>Dr. {provider.firstName} {provider.lastName}</h3>
								<em>{provider.fieldName}</em>
								<small>Appointment Fee: &#8369;{provider.appointmentFee}</small>
							</div>
						</div>
					))}
				</div>
			</div>
		</Fragment>
	)
}

export default AppointmentPatient