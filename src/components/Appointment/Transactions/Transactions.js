import React, { useEffect, useState } from "react"
import Swal from "sweetalert2"
import "./Transactions.css"

import { URL } from "../../../config"

const Transaction = () => {
	const [appointments, setAppointments] = useState([])

	useEffect( () => {
		fetch(`${URL}/appointments`, {
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setAppointments(data)
		})
	}, [appointments])

	const cancel = (appointmentId) => {
		fetch(`${URL}/appointments/${appointmentId}/cancel`, {
			method: "PUT",
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: 'success',
					title: data.message
				})
			} else {
				Swal.fire({
					icon: 'error',
					title: data.message
				})
			}
		})
	}


	return (
		<div className="content">
			<h2 className="content-title">Your appointments</h2>
			<table className="appointments-table">
				<thead>
					<tr>
						<th>Date Created</th>
						<th>Date of Appointment</th>
						<th>Doctor</th>
						<th>Appointment Fee</th>
						<th>Payment Mode</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				{ appointments.map( appointment => (
					<tr key={appointment._id}>
						<td>
							{
							new Date(appointment.dateCreated).toDateString()
							}
						</td>
						<td>
							{
							new Date(appointment.dateOfAppointment).toDateString() + " " +  
							new Date(appointment.dateOfAppointment).getHours() + ":00" 
							}
						</td>
						<td>{`Dr. ${appointment.provider.firstName} ${appointment.provider.lastName}`}</td>
						<td>&#8369; {appointment.appointmentFee}.00</td>
						<td>{appointment.paymentMode}</td>
						<td>{appointment.status}</td>
						{appointment.status === "Pending" ? 
							<td>
								<button onClick={ () => cancel(appointment._id)} className="button-action-1">Cancel</button>
							</td> :
							<td></td>
						}
					</tr>
				))}
				</tbody>
			</table>
		</div>
	)
}

export default Transaction