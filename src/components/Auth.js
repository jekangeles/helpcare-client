import React from "react"
import { Link } from "react-router-dom"

const Auth = () => {
	return (
		<h2 className="content">
			You have no access to this page. Try refreshing this page. If it doesn't work, please <Link to='/login'>login</Link> or <Link to='/register'>register</Link>.
		</h2>
	)
}
export default Auth