import React from "react"
import { Link } from "react-router-dom"

import DrawerToggleButton from "../SideDrawer/DrawerToggleButton"
import "./Toolbar.css"

const Toolbar = ({setSideDrawerOpen}) => {

	return (
		<header className="toolbar">
			<nav className="toolbar__navigation">
				<div className="toolbar__logo">
					<Link to='/'>
						help <i className="fas fa-hands-helping"></i> care
					</Link>
				</div>
				<div className="spacer" />
				<div className="toolbar__toggle-button">
					<DrawerToggleButton setSideDrawerOpen={setSideDrawerOpen}/>
				</div>
				<div className="toolbar__navigation-items">
					<ul>
						<li><Link to='/'>Home</Link></li>
						<li><Link to='/about-patients'>For Patients</Link></li>
						<li><Link to='/about-providers'>For Providers</Link></li>
						<li><Link to='/login'>Login</Link></li>
						<li><Link to='/register'>Register</Link></li>
					</ul>
				</div>
			</nav>
		</header>
	)
}

export default Toolbar