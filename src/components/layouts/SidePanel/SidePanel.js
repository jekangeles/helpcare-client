import React, { Fragment } from "react"
import { Link } from "react-router-dom"
import "./SidePanel.css"
import jwt_decode from "jwt-decode"

import { URL } from "../../../config"

const SidePanel = ({logoutHandler}) => {
	const user = jwt_decode(localStorage.getItem("token"))

	let navlinks;

	if (user.role === "patient") {
		navlinks = (
			<Fragment>
				<li>
					<Link to='/patients/dashboard'>
						<i className="fas fa-home fa-2x"></i>
						<span>Home</span>
					</Link>
				</li>
				<li>
					<Link to='/patients/dashboard/catalog'>
						<i className="fas fa-stethoscope fa-2x"></i>
						<span>Checkup</span>
					</Link>
				</li>
				<li>
					<Link to='/patients/dashboard/profile'>
						<i className="fas fa-user fa-2x"></i>
						<span>Profile</span>
					</Link>
				</li>
				<li>
					<Link onClick={ () => logoutHandler()}>
						<i className="fas fa-sign-out-alt fa-2x"></i>
						<span>Logout</span>
					</Link>
				</li>
			</Fragment>
		)
	} else if (user.role === "provider") {
		navlinks = (
			<Fragment>
				<li>
					<Link to='/providers/dashboard/appointments'>
						<i className="fas fa-stethoscope fa-2x"></i>
						<span>Appointments</span>
					</Link>
				</li>
				<li>
					<Link to='/providers/dashboard/profile'>
						<i className="fas fa-user fa-2x"></i>
						<span>Profile</span>
					</Link>
				</li>
				<li>
					<Link onClick={ () => logoutHandler()}>
						<i className="fas fa-sign-out-alt fa-2x"></i>
						<span>Logout</span>
					</Link>
				</li>
			</Fragment>
		)
	} else if (user.role === "admin") {
		navlinks = (
			<Fragment>
				<li>
					<Link to='/admin/dashboard/appointments'>
						<i className="fas fa-stethoscope fa-2x"></i>
						<span>Appointments</span>
					</Link>
				</li>
				<li>
					<Link to='/admin/dashboard/add-field'>
						<i className="fas fa-medkit fa-2x"></i>
						<span>Add Field</span>
					</Link>
				</li>
				<li>
					<Link onClick={ () => logoutHandler()}>
						<i className="fas fa-sign-out-alt fa-2x"></i>
						<span>Logout</span>
					</Link>
				</li>
			</Fragment>
		)
	}

	return (
		<nav className="side-panel">
			<div className="profile-pic-container">
				<img src={`${URL}${user.image}`} alt="user patient"/>
			</div>
			<ul>
				{navlinks}
			</ul>
		</nav>
	)
}

export default SidePanel