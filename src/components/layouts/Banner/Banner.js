import React from "react"
import "./Banner.css"

const Banner = () => {
	return (
		<div className="banner-container">
			<div className="banner">
				<span>help <i className="fas fa-hands-helping"></i> care</span>
			</div>
		</div>
	)
}

export default Banner