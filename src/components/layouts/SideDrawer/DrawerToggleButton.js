import React from "react"
import "./DrawerToggleButton.css"

const DrawerToggleButton = ({setSideDrawerOpen}) => {

	return (
		<button className="toggle-button" onClick={ () => setSideDrawerOpen(true) }>
			<div className="toggle-button__line" />
			<div className="toggle-button__line" />
			<div className="toggle-button__line" />
		</button>
	)
}

export default DrawerToggleButton