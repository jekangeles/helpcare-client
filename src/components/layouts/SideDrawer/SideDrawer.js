import React from "react"
import { Link } from "react-router-dom"
import "./SideDrawer.css"

const SideDrawer = ({sideDrawerOpen, setSideDrawerOpen}) => {
	
	let drawerClasses = 'side-drawer'
	if(sideDrawerOpen) {
		drawerClasses = 'side-drawer open'
	}

	return (
		<nav className={drawerClasses}>
			<ul>
				<div>
					<span>NAVIGATION</span>
					<li><Link onClick={ () => setSideDrawerOpen(false) }to='/'>Home</Link></li>
					<li><Link onClick={ () => setSideDrawerOpen(false) }to='/about-patients'>For Patients</Link></li>
					<li><Link onClick={ () => setSideDrawerOpen(false) }to='/about-providers'>For Providers</Link></li>
					<li><Link onClick={ () => setSideDrawerOpen(false) }to='/contact'>Contact</Link></li>
				</div>
				<div>
					<span>ACCOUNT</span>
					<li><Link onClick={ () => setSideDrawerOpen(false) }to='/login'>Login</Link></li>
					<li><Link onClick={ () => setSideDrawerOpen(false) }to='/register'>Register</Link></li>
				</div>
			</ul>
		</nav>
	)
}

export default SideDrawer