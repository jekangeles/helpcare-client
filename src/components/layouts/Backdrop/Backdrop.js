import React from "react"

import "./Backdrop.css"
const Backdrop = ({setSideDrawerOpen}) => {

	return (
		<div className="backdrop" onClick={ () => setSideDrawerOpen(false)}/>
	)
}

export default Backdrop