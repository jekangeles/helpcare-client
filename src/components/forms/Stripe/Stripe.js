import React from "react"
import StripeCheckout from "react-stripe-checkout"
import { PUBLISHABLE_KEY } from "../../../config"
import { URL } from "../../../config"
import Swal from "sweetalert2"

const Stripe = ({ provider, dateOfAppointment}) => {
	const checkout = (token) => {
		let body = {
			token: token, 
			amount: provider.appointmentFee,
			provider: provider,
			dateOfAppointment: dateOfAppointment
		}
		// alert(token)

		fetch(`${URL}/appointments/stripe`, {
			method: "POST",
			body: JSON.stringify(body),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: 'success',
					title: data.message
				})
				window.location.href="/patients/dashboard"
			} else {
				Swal.fire({
					icon: 'error',
					title: data.message
				})
			}
		})
	}

	return(
		<StripeCheckout
			stripeKey={PUBLISHABLE_KEY}
			label="Card Payment"
			name="HelpCare"
			description="Connecting patients and doctors"
			panelLabel="Submit"
			billingAddress={false}
			currency="PHP"
			allowRememberMe={false}
			token={checkout}
		/>
	)

}

export default Stripe