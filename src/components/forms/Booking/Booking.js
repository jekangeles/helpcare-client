import React, { Fragment, useEffect, useState } from "react"
import { useParams, Redirect } from "react-router-dom"
import "./Booking.css"
import Swal from "sweetalert2"
import Stripe from "../Stripe/Stripe"

import { URL } from "../../../config"

const Booking = () => {
	let {providerId} = useParams()
	const [provider, setProvider] = useState({})
	// const patient = JSON.parse(localStorage.getItem("user"))
	const [isCompleted, setIsCompleted] = useState (false)

	const [formData, setFormData] = useState({})
	useEffect( () => {
		fetch(`${URL}/providers/${providerId}`)
		.then(res => res.json())
		.then(data => {
			setProvider(data)
		})
	}, [])

	const onChangeHandler = (e) => {
		setFormData({
			...formData, 
			[e.target.name]: e.target.value
		})
		console.log(formData)
	}

	const checkout = () => {

		// alert(JSON.stringify(provider))
		// alert(JSON.stringify(patient))

		let body = new FormData()
		body.provider = provider
		body.dateOfAppointment = formData.dateOfAppointment

		fetch(`${URL}/appointments`, {
			method: "POST",
			body: JSON.stringify(body),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: "success", 
					title: data.message,
				})
				// window.location.href="/patients/dashboard"
				setIsCompleted(true)
			} else {
				Swal.fire({
					icon: 'error',
					title: data.message,
				})
			}
		})
	}

	let now = new Date().toISOString().split(':')[0]+":00"
	// console.log(now)
	return (
		<Fragment>
			<div className="content">
				<div className="content-title">
					<h2>Schedule an appointment with Dr. {provider.firstName} {provider.lastName}</h2>
				</div>
				<div className="booking-form-container">
					<form className="booking-form">
						<input type="datetime-local" name="dateOfAppointment" step="3600" min={now} onChange={onChangeHandler} />
						<span>Appointment Fee is  &#8369;{provider.appointmentFee}</span>
						<button type="button" className="button-book button-cash" onClick={checkout}>Cash</button>
						<Stripe amount={provider.appointmentFee*100} provider={provider} dateOfAppointment={formData.dateOfAppointment}/>
					</form>
				</div>
				{
					isCompleted ?
					<Redirect to="/patients/dashboard" />
					: null
				}
			</div>
		</Fragment>
	)
}

export default Booking