import React, { useState, useEffect, Fragment } from "react"
import "./Register.css"
import Swal from "sweetalert2"

import { URL } from "../../../config"
import { Redirect } from "react-router-dom"

const Register = () => {
	const [registerAsPatient, setRegisterAsPatient] = useState(true)
	const [formData, setFormData] = useState({})
	const [fields, setFields] = useState([])

	const [isCompleted, setIsCompleted] = useState(false)

	useEffect( () => {
		fetch(`${URL}/fields`)
		.then(res => res.json())
		.then(data => {
			setFields(data)
		})
	}, [])

	const onChangeHandler = (e) => {
		setFormData({
			...formData, 
			[e.target.name]: e.target.value
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		if(registerAsPatient) {
			fetch(`${URL}/patients/register`, {
				method: "POST",
				body: JSON.stringify(formData),
				headers: {
					"Content-Type": "application/json"
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data.status === 200) {
					Swal.fire({
						icon: 'success',
						text: data.message
					})
				} else {
					Swal.fire({
						icon: 'error', 
						text: data.message
					})
				}
			})
			setIsCompleted(true)
		} else {
			fetch(`${URL}/providers/register`, {
				method: "POST",
				body: JSON.stringify(formData),
				headers: {
					"Content-Type": "application/json"
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data.status === 200) {
					Swal.fire({
						icon: 'success',
						text: data.message
					})
				} else {
					Swal.fire({
						icon: 'error',
						text: data.message
					})
				}
			})
			setIsCompleted(true)
		}
	}

	return (
		<div className="register">
			<div className="register__box">
				<div className="register__tabs">
					<div 
						className={registerAsPatient ? "register__tab1 active" : "register__tab1"}
						onClick={ () => setRegisterAsPatient(true)}
					>
						<span>Register as Patient</span>
					</div>
					<div 
						className={registerAsPatient ? "register__tab2" : "register__tab2 active"}
						onClick={ () => setRegisterAsPatient(false)}
					>
						<span>Register as Provider</span>
					</div>
				</div>
				<div className="reg-form__box">
					<form className="register__form" onSubmit={onSubmitHandler}>
						<div>
							<input className="input-field" type="text" name="firstName" placeholder="First Name" onChange={onChangeHandler}/>
							<input className="input-field" type="text" name="lastName" placeholder="Last Name" onChange={onChangeHandler}/>
							<input className="input-field" type="text" name="email" placeholder="Email Address" onChange={onChangeHandler}/>
							<input className="input-field" type="password" name="password" placeholder="Password" onChange={onChangeHandler}/>
							<input className="input-field" type="password" name="password2" placeholder="Confirm Password" onChange={onChangeHandler}/>
							{ registerAsPatient === false ? 
								<Fragment>
									<select defaultValue="Select Medical Field" className="input-field" name="fieldName" onChange={onChangeHandler}>
										<option disabled>Select Medical Field</option>
										{ fields.map(field => (
											<option value={field.name} key={field._id}>{field.name}</option>
										))}
									</select>
									<input className="input-field" type="text" name="clinicName" placeholder="Clinic Name" onChange={onChangeHandler} />
									<input className="input-field input-address" type="text" name="clinicAddress" placeholder="Clinic Address" onChange={onChangeHandler}/>
									<input className="input-field" type="number" name="appointmentFee" placeholder="Flat Appointment Fee" step="50" onChange={onChangeHandler}/>
								</Fragment>
								: null
							}
							<button className="form-button">Register</button>
						</div>
					</form>
					{
						isCompleted ? 
						<Redirect to="/login" />
						: null
					}
				</div>
			</div>
		</div>
	)
}

export default Register