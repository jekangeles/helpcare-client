import React, { useState, useEffect } from "react"
import Swal from "sweetalert2"

import { URL } from "../../../config"

const AddField = () => {

	const [fields, setFields] = useState([])

	useEffect( () => {
		fetch(`${URL}/fields`)
		.then(res => res.json())
		.then(data => {
			setFields(data)
		})
	}, [fields])

	const [formData, setFormData] = useState({})

	const onChangeHandler = (e) => {
		setFormData({
			...formData, 
			[e.target.name]: e.target.value
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		fetch(`${URL}/fields`, {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data.status === 200) {
				Swal.fire({
					icon: 'success',
					title: data.message
				})
			} else {
				Swal.fire({
					icon: 'error',
					title: data.message
				})
			}
		})
	}

	return (
		<div className="content">
			<h2 className="content-title">Add Field</h2>
			<form onSubmit={onSubmitHandler}>
				<input  className="content-title" type="text" name="name" onChange={onChangeHandler} />
				<button className="button-action-1" type="submit" >Add</button>
			</form>
			<h3 className="content-title">Fields: </h3>
			<ul className="content-title">
				{ fields.map( field => (
					<li>{field.name}</li>
				))}
			</ul>
		</div>
	)
}

export default AddField