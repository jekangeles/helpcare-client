import React, { useState } from "react"
import "./Login.css"
import Swal from "sweetalert2"

import { URL } from "../../../config"
import { Redirect } from "react-router-dom"

const Login = () => {
	const [loginAsPatient, setLoginAsPatient] = useState(true)
	const [formData, setFormData] = useState({})

	const [isCompleted, setIsCompleted] = useState(false)

	const onChangeHandler = (e) => {
		setFormData({
			...formData, 
			[e.target.name]: e.target.value
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		if(loginAsPatient) {
			// alert(JSON.stringify(formData))
			fetch(`${URL}/patients/login`, {
				method: "POST",
				body: JSON.stringify(formData),
				headers: {
					"Content-Type": "application/json"
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data.auth) {
					Swal.fire({
						icon: 'success',
						title: data.message,
						showConfirmButton: false,
						timer: 2000,
						timerProgressBar: true
					})
					localStorage.setItem('user', JSON.stringify(data.user))
					localStorage.setItem('token', data.token)
					// if(data.user.role === "admin") {
					// 	window.location.href="/admin/dashboard/appointments"
					// } else {
					// 	window.location.href="/patients/dashboard"
					// }
					setIsCompleted(true)
				} else {
					Swal.fire({
						icon: 'error',
						title: data.message,
						showConfirmButton: false,
						timerProgressBar: true
					})
				}
			})

		} else {
			fetch(`${URL}/providers/login`, {
				method: "POST",
				body: JSON.stringify(formData),
				headers: {
					"Content-Type": "application/json"
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data.auth) {
					Swal.fire({
						icon: 'success',
						title: data.message,
						showConfirmbutton: false,
						timer: 2000,
						timerProgressBar: true
					})
					localStorage.setItem('user', JSON.stringify(data.user))
					localStorage.setItem('token', data.token)
					// if(data.user.role === "admin"){
					// 	window.location.href="/admin/dashboard/appointments"
					// } else {
					// 	window.location.href="/providers/dashboard/appointments"
					// }
					setIsCompleted(true)
				} else {
					Swal.fire({
						icon: 'error',
						title: data.message,
						showConfirmbutton: false,
						timerProgressBar: true
					})
				}
			})
		}
	}

	return (
		<div className="login">
			<div className="login__box">
				<div className="login__tabs">
					<div 
						className={loginAsPatient ? "login__tab1 active" : "login__tab1"}
						onClick={ () => setLoginAsPatient(true)}
					>
						<span>Login as Patient</span>
					</div>
					<div 
						className={loginAsPatient ? "login__tab2" : "login__tab2 active"}
						onClick={ () => setLoginAsPatient(false)}
					>
						<span>Login as Provider</span>
					</div>
				</div>
				<div className="log-form__box">
					<form className="login__form" onSubmit={onSubmitHandler}>
						<div>
							<input className="input-field" type="text" name="email" placeholder="Email Address" onChange={onChangeHandler}/>
							<input className="input-field" type="password" name="password" placeholder="Password" onChange={onChangeHandler}/>
							<button className="form-button">Log in</button>
						</div>
					</form>
				</div>
			</div>
			{
			isCompleted ? 
				JSON.parse(localStorage.getItem("user")).role === "admin" ?
				<Redirect to="/admin/dashboard/appointments" />
				: JSON.parse(localStorage.getItem("user")).role === "patient" ?
					<Redirect to="/patients/dashboard" />
					: <Redirect to="/providers/dashboard/appointments" />
			: null
			}
		</div>
	)
}

export default Login