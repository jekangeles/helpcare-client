import React, { useState } from "react"
import Swal from "sweetalert2"

import { URL } from "../../../../config"

const EditProfilePatient = ({setEditing, patient}) => {
	const [formData, setFormData] = useState({
		...patient
	})

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		setFormData({
			...formData, 
			image: e.target.files[0]
		})
	}

	const onSubmitHandler = (e) => {
		// alert(JSON.stringify(formData))
		let updatedPatient = new FormData()
		updatedPatient.append('firstName', formData.firstName)
		updatedPatient.append('lastName', formData.lastName)
		updatedPatient.append('email', formData.email)

		if(typeof formData.image === "object") {
			updatedPatient.append('image', formData.image)
		}

		fetch(`${URL}/patients/${patient._id}`, {
			method: "PUT",
			body: updatedPatient,
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				icon: 'success',
				text: data.message
			})
			setEditing(false)
		})
	}

	return (
		<div className="profile-table-container">
			<table className="profile-table">
				<tbody>
					<tr>
						<th>First Name</th>
						<td><input type="text" value={formData.firstName} name="firstName" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Last Name</th>
						<td><input type="text" value={formData.lastName} name="lastName" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Email</th>
						<td><input type="text" value={formData.email} name="email" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Profile Picture</th>
						<td><input type="file" name="email" onChange={handleFile} /></td>
					</tr>
				</tbody>
			</table>
			<small>*Changes will be made the next time you log in</small>
			<div className="profile-buttons-container">
				<button onClick={ () => onSubmitHandler() } className="button-book button-profile">Submit</button>
				<button onClick={ () => setEditing(false)} className="button-book button-profile">Cancel</button>
			</div>
		</div>
	)
}

export default EditProfilePatient