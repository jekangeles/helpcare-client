import React, { useState, useEffect } from "react"
import Swal from "sweetalert2"

import { URL } from "../../../../config"

const EditProfileProvider = ({setEditing, provider}) => {
	const [formData, setFormData] = useState({
		...provider
	})

	const [fields, setFields] = useState([])

	useEffect( () => {
		fetch(`${URL}/fields`)
		.then(res => res.json())
		.then(data => {
			setFields(data)
		})
	})

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		setFormData({
			...formData, 
			image: e.target.files[0]
		})
	}

	const onSubmitHandler = (e) => {
		// alert(JSON.stringify(formData))
		let updatedProvider = new FormData()
		updatedProvider.append('firstName', formData.firstName)
		updatedProvider.append('lastName', formData.lastName)
		updatedProvider.append('email', formData.email)
		updatedProvider.append('fieldName', formData.fieldName)
		updatedProvider.append('appointmentFee', formData.appointmentFee)
		updatedProvider.append('clinicName', formData.clinicName)
		updatedProvider.append('clinicAddress', formData.clinicAddress)

		if(typeof formData.image === "object") {
			updatedProvider.append('image', formData.image)
		}

		fetch(`${URL}/providers/${provider._id}`, {
			method: "PUT",
			body: updatedProvider,
			headers: {
				"x-auth-token": localStorage.getItem('token')
			}
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				icon: 'success',
				text: data.message
			})
			setEditing(false)
		})
	}

	return (
		<div className="profile-table-container">
			<table className="profile-table">
				<tbody>
					<tr>
						<th>First Name</th>
						<td><input type="text" value={formData.firstName} name="firstName" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Last Name</th>
						<td><input type="text" value={formData.lastName} name="lastName" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Email</th>
						<td><input type="text" value={formData.email} name="email" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Profile Picture</th>
						<td><input type="file" name="email" onChange={handleFile} /></td>
					</tr>
					<tr>
						<th>Field</th>
						<td>
							<select name="fieldName" onChange={onChangeHandler}>
								{ fields.map( field => (
									field.name === formData.fieldName ?
									<option selected key={field._id}>{field.name}</option>
									: <option key={field._id}>{field.name}</option>
								))}
							</select>
						</td>
					</tr>
					<tr>
						<th>Fee</th>
						<td><input type="number" value={formData.appointmentFee} name="appointmentFee" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Clinic Name</th>
						<td><input type="text" value={formData.clinicName} name="clinicName" onChange={onChangeHandler} /></td>
					</tr>
					<tr>
						<th>Clinic Address</th>
						<td><input type="text" value={formData.clinicAddress} name="clinicAddress" onChange={onChangeHandler} /></td>
					</tr>
				</tbody>
			</table>
			<small>*Changes will be made the next time you log in</small>
			<div className="profile-buttons-container">
				<button onClick={ () => onSubmitHandler() } className="button-book button-profile">Submit</button>
				<button onClick={ () => setEditing(false)} className="button-book button-profile">Cancel</button>
			</div>
		</div>
	)
}

export default EditProfileProvider