import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import SidePanel from "../../layouts/SidePanel/SidePanel"
import Banner from "../../layouts/Banner/Banner"
import AddField from "../../forms/AddField/AddField"

import { URL } from "../../../config"

const DashboardAdmin = ({logoutHandler}) => {
	const [appointments, setAppointments] = useState([])

	useEffect( () => {
		fetch(`${URL}/appointments/admin`, {
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setAppointments(data)
			// console.log(data)
		})
	})

	return (
		<Router>
			<Banner />
			<SidePanel 
				logoutHandler={logoutHandler}
			/>
			<Switch>
				<Route path="/admin/dashboard/add-field">
					<AddField />
				</Route>
				<Route path="/admin/dashboard/appointments">
					<div className="content">
						<h1 className="content-title">All appointments</h1>
						<table className="appointments-table">
							<thead>
								<tr>
									<th>Patient</th>
									<th>Doctor</th>
									<th>Date Created</th>
									<th>Date of Appointment</th>
									<th>Payment Mode</th>
									<th>Appointment Fee</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							{ appointments.map( appointment => (
								<tr key={appointment._id}>
									<td>{`${appointment.patient.firstName} ${appointment.patient.lastName}`}</td>
									<td>{`Dr. ${appointment.provider.firstName} ${appointment.provider.lastName}`}</td>
									<td>
										{
										new Date(appointment.dateCreated).toDateString()
										}
									</td>
									<td>
										{
										new Date(appointment.dateOfAppointment).toDateString() + " " +  
										new Date(appointment.dateOfAppointment).getHours() + ":00" 
										}
									</td>
									<td>{appointment.paymentMode}</td>
									<td>&#8369; {appointment.appointmentFee}.00</td>
									<td>{appointment.status}</td>
								</tr>
							))}
							</tbody>
						</table>
					</div>
				</Route>
			</Switch>
		</Router>
	)
}

export default DashboardAdmin