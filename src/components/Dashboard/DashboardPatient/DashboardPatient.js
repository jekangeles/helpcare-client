import React, { Fragment, useState, useEffect } from "react"
import { Link, BrowserRouter as Router, Switch, Route } from "react-router-dom"
import SidePanel from "../../layouts/SidePanel/SidePanel"
import Banner from "../../layouts/Banner/Banner"
import "./DashboardPatient.css"

import Provider from "../../Provider/Provider"
import Auth from "../../Auth"
import Catalog from "../../Appointment/Catalog/Catalog"
import Booking from "../../forms/Booking/Booking"
import ProfilePatient from "../../Profile/ProfilePatient/ProfilePatient"
import Transaction from "../../Appointment/Transactions/Transactions"

import { URL } from "../../../config"

const DashboardPatient = ({logoutHandler, user, token}) => {

	const [upcomingAppointments, setUpcomingAppointments] = useState([])
	const [appointmentHistory, setAppointmentHistory] = useState([])

	useEffect( () => {

		fetch(`${URL}/appointments/upcoming`, {
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setUpcomingAppointments(data)
			// console.log(data)
		})

		fetch(`${URL}/appointments/history`, {
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setAppointmentHistory(data)
		})

	}, [upcomingAppointments, appointmentHistory])

	return (
		<Router>
			<Banner />
			<SidePanel 
				logoutHandler={logoutHandler}
			/>
			<Switch>
            	<Route path="/patients/dashboard/catalog">
            		{user && token ? <Catalog /> : <Auth /> }
            	</Route>
            	<Route path="/patients/dashboard/book/:providerId" >
            		{user && token ? <Booking /> : <Auth /> }
            	</Route>
            	<Route path="/patients/dashboard/providers/:providerId">
            		{user && token ? <Provider /> : <Auth /> }
            	</Route>
            	<Route path="/patients/dashboard/history">
            		{user && token ? <Transaction /> : <Auth /> }
            	</Route>
            	<Route path="/patients/dashboard/profile">
            		{user && token ? <ProfilePatient user={user}/> : <Auth /> }
            	</Route>
            	<Route exact path="/patients/dashboard">
					<Fragment>
						<div className="content">
							<div className="content-title">
								<h2>Dashboard</h2>
							</div>
							<div className="content-body">
								<div className="upcoming-container">
									<h3>Your upcoming appointments</h3>
									<hr/>
									<div className="content-table">
										<table>
											<thead>
												<tr>
													<th>Date of Appointment</th>
													<th>Doctor</th>
													<th>Fee</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
												{
													upcomingAppointments.length ?
													upcomingAppointments.map( appointment => (
													<tr key={appointment._id}>
														<td>
														{
															new Date(appointment.dateOfAppointment).toDateString() + " " +  
															new Date(appointment.dateOfAppointment).getHours() + ":00" 
														}
														</td>
														<td>Dr. {appointment.provider.firstName} {appointment.provider.lastName}</td>
														<td>&#8369; {appointment.appointmentFee}</td>
														<td>{appointment.status}</td>
													</tr>
												))
													: <tr><td colSpan="4">No items to show</td></tr>
												}
											</tbody>
										</table>
										<button className="button-book button-view-details"><Link to="/patients/dashboard/history">View details</Link></button>
									</div>
								</div>
								<div className="history-container">
									<h3>Your appointment history</h3>
									<hr/>
									<div className="content-table">
										<table>
											<thead>
												<tr>
													<th>Date of Appointment</th>
													<th>Doctor</th>
													<th>Fee</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
												{
													appointmentHistory.length ?
													appointmentHistory.map( appointment => (
													<tr key={appointment._id}>
														<td>
														{
															new Date(appointment.dateOfAppointment).toDateString() + " " +  
															new Date(appointment.dateOfAppointment).getHours() + ":00" 
														}
														</td>
														<td>Dr. {appointment.provider.firstName} {appointment.provider.lastName}</td>
														<td>&#8369; {appointment.appointmentFee}</td>
														<td>{appointment.status}</td>
													</tr>
												))
													: <tr><td colSpan="4">No items to show</td></tr>
												}
											</tbody>
										</table>
										<button className="button-book button-view-details"><Link to="/patients/dashboard/history">View details</Link></button>
									</div>
								</div>
							</div>
						</div>
					</Fragment>
				</Route>
			</Switch>
		</Router>
	)
}

export default DashboardPatient