import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Swal from "sweetalert2"
import SidePanel from "../../layouts/SidePanel/SidePanel"
import Banner from "../../layouts/Banner/Banner"

import { URL } from "../../../config"

import ProfileProvider from "../../Profile/ProfileProvider/ProfileProvider"

const DashboardProvider = ({logoutHandler, user, token}) => {
	const [appointments, setAppointments] = useState([])

	useEffect( () => {
		fetch(`${URL}/appointments/provider`, {
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setAppointments(data)
		})
	}, [appointments])

	const accept = (appointmentId) => {
		// alert(appointmentId)
		fetch(`${URL}/appointments/${appointmentId}/accept`, {
			method: "PUT",
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: 'success',
					title: data.message
				})
			} else {
				Swal.fire({
					icon: 'error',
					title: data.message
				})
			}
		})
	}

	const reject = (appointmentId) => {
		// alert(appointmentId)
		fetch(`${URL}/appointments/${appointmentId}/reject`, {
			method: "PUT",
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: 'success',
					title: data.message
				})
			} else {
				Swal.fire({
					icon: 'error',
					title: data.message
				})
			}
		})
	}

	const complete = (appointmentId) => {
		// alert(appointmentId)
		fetch(`${URL}/appointments/${appointmentId}/complete`, {
			method: "PUT",
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: 'success',
					title: data.message
				})
			} else {
				Swal.fire({
					icon: 'error',
					title: data.message
				})
			}
		})
	}





	return (
		<Router>
			<Banner />
			<SidePanel 
				logoutHandler={logoutHandler}
			/>
			<Switch>
				<Route path="/providers/dashboard/profile">
					<ProfileProvider user={user}/>
				</Route>
				<Route path="/providers/dashboard/appointments">
					<div className="content">
						<h2 className="content-title">Your Appointments</h2>
						<table className="appointments-table">
							<thead>
								<tr>
									<th>Date Created</th>
									<th>Date of Appointment</th>
									<th>Patient</th>
									<th>Payment Mode</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							{ 
								appointments.length ?
								appointments.map( appointment => (
								<tr key={appointment._id}>
									<td>
										{
										new Date(appointment.dateCreated).toDateString()
										}
									</td>
									<td>
										{
										new Date(appointment.dateOfAppointment).toDateString() + " " +  
										new Date(appointment.dateOfAppointment).getHours() + ":00" 
										}
									</td>
									<td>{`${appointment.patient.firstName} ${appointment.patient.lastName}`}</td>
									<td>{appointment.paymentMode}</td>
									<td>{appointment.status}</td>
									{appointment.status === "Pending" ? 
										<td>
											<button onClick={ () => accept(appointment._id) }className="button-action-2">Accept</button>
											<button onClick={ () => reject(appointment._id) }className="button-action-3">Reject</button>
										</td> :
										appointment.status === "Accepted" ?
										<td>
											<button onClick={ () => complete(appointment._id) } className="button-action-4">Complete</button>
										</td> :
										<td>
										</td>
									}
								</tr>
							))
								: <tr><td colSpan="6">No items to show</td></tr>
							}
							</tbody>
						</table>
					</div>
				</Route>
			</Switch>
		</Router>
	)
}

export default DashboardProvider