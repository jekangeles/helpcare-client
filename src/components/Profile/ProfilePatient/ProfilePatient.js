import React, { useState } from "react"
import "./ProfilePatient.css"
import EditProfilePatient from "../../forms/EditProfile/EditProfilePatient/EditProfilePatient"

import { URL } from "../../../config"

const ProfilePatient = ({user}) => {
	const [editing, setEditing] = useState(false)

	return (
		<div className="content">
			<h1 className="content-title">My Profile</h1>
			<div className="profile-container">
				<div className="profile-pic-container-2">
					<img src={`${URL}${user.image}`} alt="patient-profile-pic"/>
				</div>
				{ editing ? 
					<EditProfilePatient setEditing={setEditing} patient={user}/>
				:
				<div className="profile-table-container">
					<table className="profile-table">
						<tbody>
							<tr>
								<th>First Name</th>
								<td>{user.firstName}</td>
							</tr>
							<tr>
								<th>Last Name</th>
								<td>{user.lastName}</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>{user.email}</td>
							</tr>
						</tbody>
					</table>
					<div className="profile-buttons-container">
						<button onClick={ () => setEditing(true)}className="button-book button-profile">Edit Profile</button>
					</div>
				</div>
				}
			</div>
		</div>
	)
}

export default ProfilePatient