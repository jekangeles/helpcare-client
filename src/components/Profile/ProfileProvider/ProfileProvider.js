import React, { useState } from "react"

import EditProfileProvider from "../../forms/EditProfile/EditProfileProvider/EditProfileProvider"

import { URL } from "../../../config"

const ProfileProvider = ({user}) => {
	const [editing, setEditing] = useState(false)

	return (
		<div className="content">
			<h1 className="content-title">My Profile</h1>
			<div className="profile-container">
				<div className="profile-pic-container-2">
					<img src={`${URL}${user.image}`} alt="patient-profile-pic"/>
				</div>
				{ editing ? 
					<EditProfileProvider setEditing={setEditing} provider={user}/>
					// null
				:
				<div className="profile-table-container">
					<table className="profile-table">
						<tbody>
							<tr>
								<th>First Name</th>
								<td>{user.firstName}</td>
							</tr>
							<tr>
								<th>Last Name</th>
								<td>{user.lastName}</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>{user.email}</td>
							</tr>
							<tr>
								<th>Field</th>
								<td>{user.fieldName}</td>
							</tr>
							<tr>
								<th>Fee</th>
								<td>&#8369; {user.appointmentFee}.00</td>
							</tr>
							<tr>
								<th>Clinic Name</th>
								<td>{user.clinicName}</td>
							</tr>
							<tr>
								<th>Clinic Address</th>
								<td>{user.clinicAddress}</td>
							</tr>
						</tbody>
					</table>
					<div className="profile-buttons-container">
						<button onClick={ () => setEditing(true)}className="button-book button-profile">Edit Profile</button>
					</div>
				</div>
				}
			</div>
		</div>
	)
}

export default ProfileProvider