import React from "react"
import "./LandingPage.css"
import { Link } from "react-router-dom"

const LandingPage = () => {
	return(
		<div className="segment1">
			<div className="segment1__text">
				<h1>Book your appointments from the comfort of your home.</h1>
				<p>We know you hate waiting rooms, so we've made booking checkups simple for you. <Link to='/register'>Sign up</Link> now to see how easy it is!</p>
			</div>
		</div>
	)
}

export default LandingPage